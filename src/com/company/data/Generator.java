package com.company.data;

import com.company.model.Smartphone;
import com.company.model.Store;

public class Generator {
    private static int numberOfStores = 3;

    private Generator() {
    }

    public static Store[] generate() {
        Store[] stores = new Store[numberOfStores];
        {
            Smartphone[] smartphone = {
                    new Smartphone("Apple", "iPhone XS", 20000),
                    new Smartphone("Samsung", "S20", 22000),
                    new Smartphone("Apple", "iPhone 11", 25000),
                    new Smartphone("Samsung", "J5", 5000),
                    new Smartphone("Xiaomi", "P30", 10000)
            };
            stores[0] = new Store("Comfy", "м.Харків, вулиця Героїв Праці, 27", smartphone);
        }
        {
            Smartphone[] smartphone = {
                    new Smartphone("Apple", "iPhone XS", 21000),
                    new Smartphone("Samsung", "S20", 23500),
                    new Smartphone("Xiaomi", "P30", 11000),
                    new Smartphone("Samsung", "J5", 5200),
                    new Smartphone("Apple", "iPhone 11 PRO", 30000)
            };
            stores[1] = new Store("Citrus", "м. Харків, вулиця Cумська, 81", smartphone);
        }
        {
            Smartphone[] smartphone = {
                    new Smartphone("Xiaomi", "Redmi 4", 10000),
                    new Smartphone("Samsung", "J5", 5000),
                    new Smartphone("Xiaomi", "P30", 11500),
                    new Smartphone("Xiaomi", "P10", 7800)
            };
            stores[2] = new Store("Алло", "м. Харків, вулиця Героїв Праці, 34", smartphone);
        }
        return stores;
    }
}
