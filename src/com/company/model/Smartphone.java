package com.company.model;

public class Smartphone {
    private String manufacturer;
    private String model;
    private int price;

    public Smartphone(String manufacturer, String model, int price) {
        this.manufacturer = manufacturer;
        this.model = model;
        this.price = price;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getModel() {
        return model;
    }

    public int getPrice() {
        return price;
    }

}

