package com.company.model;

public class Store {
    private String name;
    private String address;
    public Smartphone[] smartphones;

    public Store(String name, String address, Smartphone[] smartphones) {
        this.name = name;
        this.address = address;
        this.smartphones = smartphones;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public Smartphone cheapSmartphoneByModel(int lowestPrice, String model) {
        Smartphone cheapSmartphone = null;
        for (Smartphone smartphone : smartphones) {
            if (smartphone.getPrice() < lowestPrice && smartphone.getModel().equalsIgnoreCase(model)) {
                lowestPrice = smartphone.getPrice();
                cheapSmartphone = smartphone;
            }
        }
        return cheapSmartphone;
    }
}
