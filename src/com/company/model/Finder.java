package com.company.model;

public class Finder {
    int lowestPrice = 100000;

    Store[] stores;

    public Finder(Store[] stores) {
        this.stores = stores;
    }

    public void smartphoneByManufacturer(String manufacturer) {
        for (Store store : stores) {
            for (Smartphone smartphone : store.smartphones) {
                if (smartphone.getManufacturer().equalsIgnoreCase(manufacturer)) {
                    System.out.println((String.format("ваш сматфон: %s ", smartphone.getModel() + ", цена: " + smartphone.getPrice() + ", магазин: " + store.getName() + ", адрес: " + store.getAddress())));
                }
            }
        }
    }

    public Object smartphoneByModel(String model) {
        Store cheapestStore = storeByModel(model);
        if (cheapestStore == null) {
            return ("Смартфона нет в наличии");
        }

        Smartphone cheapSmartphone = cheapestStore.cheapSmartphoneByModel(lowestPrice, model);
        for (Store store : stores) {
            Smartphone cheapSmartphoneInStore = store.cheapSmartphoneByModel(lowestPrice, model);
            if (cheapSmartphoneInStore != null) {
                cheapSmartphone = cheapSmartphoneInStore;
                cheapestStore = store;
                lowestPrice = cheapSmartphoneInStore.getPrice();
            }
        }
        System.out.println("Смартфон с наиболее низкой ценой: " + cheapSmartphone.getManufacturer() + " " + cheapSmartphone.getModel() + ", цена: " + cheapSmartphone.getPrice() + ". В наличии в магазине: " + cheapestStore.getName() + ", адрес:" + cheapestStore.getAddress());
        return null;
    }

    private Store storeByModel(String model) {
        for (Store store : stores) {
            if (store.cheapSmartphoneByModel(lowestPrice, model) != null) {
                return store;
            }
        }
        return null;
    }
}
