package com.company;

import com.company.data.Generator;
import com.company.model.Finder;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Finder finder = new Finder(Generator.generate());

        Scanner scan = new Scanner(System.in);
        System.out.println("Введите название производителя? (Apple, Samsung, Xiaomi): ");
        String manufacturer = scan.nextLine();
        System.out.println("Введите модель телефона? (Apple: iPhone XS , iPhone 11, iPhone 11 Pro ; Samsung:  S20, j5; Xiaomi: P30, Redmi 4, P10): ");
        String model = scan.nextLine();

        finder.smartphoneByManufacturer(manufacturer);
        finder.smartphoneByModel(model);
    }
}

